import pandas as pd
import numpy as np


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("C:\\train.csv")
    return df

print(get_titatic_dataframe())
def get_filled():
    df = get_titatic_dataframe()
    '''
    Put here a code for filling missing values in titanic dataset for column 'Age' and return these values in this view - [('Mr.', x, y), ('Mrs.', k, m), ('Miss.', l, n)]
    '''
    df['Title'] = df['Name'].str.extract(r' ([A-Za-z]+)\.')

    title_groups = ['Mr.', 'Mrs.', 'Miss.']

    result = []
    for title in title_groups:
        median_age = df.loc[df['Title'] == title, 'Age'].median()
        missing_count = df.loc[(df['Title'] == title) & df['Age'].isna(), 'Age'].count()
        result.append((title, missing_count, round(median_age) if not np.isnan(median_age) else None))

    return result

filled_values = get_filled()
print(filled_values)
